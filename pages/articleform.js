import styles from './../styles/Articleform.module.css'
import {useFormik} from 'formik'
import * as Yup from 'yup'
import { useRef, useState } from 'react'
import $ from 'jquery'
function sarticle() {
    const inputRef = useRef(null)
    const [files,setFiles] = useState()
    const initialValues = {
        name:'',
        title:'',
        category:[],
        // image:'',
        content:'',
    }
    
    const onSubmit = (values,onSubmitProps)=>{
        // console.log(inputRef.current.files)
        // console.log('form-data',formik.values)
        let data = {...formik.values,files};
        console.log('data>>',data)
        onSubmitProps.setSubmitting(false)
        onSubmitProps.resetForm()
        setFiles(null)
        inputRef.current.value= null
    }
    // const validate =  values =>{
    //     let errors = {}
    //     if(!values.name){
    //         errors.name = 'Required '
    //     }
    //     if(!values.email){
    //         errors.email = 'Required '
    //     }else if(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(values.email)){
    //         email.errors = "Invalid email format"
    //     }
    //     if(!values.channel){
    //         errors.channel = 'Required '
    //     }
    //     return errors
    // }
    const validationSchema = Yup.object({
        name: Yup.string().required("Required"),
        title: Yup.string().required('Required'),
        // image: Yup.mixed().required('Required'),
        content: Yup.string().required('Required')
    })
    const formik = useFormik({
        initialValues,
        onSubmit,
        // validate
        validationSchema
    })
    const fileUpload = (e)=>{
        // console.log(e.target.files)
        setFiles(e.target.files)
    }
   
  
    // console.log('form-values',formik.values)
    return (
        <div>
            <form className={styles.form} onSubmit={formik.handleSubmit}>
                <div className="input-field">
                    <label htmlFor='name'>Name</label>
                    <input type="text" id="name" name="name" 
                    {...formik.getFieldProps('name')}
                    // onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.name}
                    />
                    {formik.errors.name&&formik.touched.name?<div className={styles.formError}>{formik.errors.name}</div>:''}
                </div>

                <div className="input-field">
                    <label htmlFor='title'>Title</label> 
                    <input type="text" id="title" name="title"
                    //  onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.email}
                    {...formik.getFieldProps('title')}
                    />
                    {formik.errors.title&&formik.touched.title?<div className={styles.formError}>{formik.errors.title}</div>:''}
                </div>
                
                <div className="input-field">
                    <label htmlFor="category">Materialize Multiple Select</label>
                    <select id="category" name="category" multiple {...formik.getFieldProps('category')}>
                    <option value="1">Option 1</option>
                    <option value="2">Option 2</option>
                    <option value="3">Option 3</option>
                    </select>
                </div>
                <br/><br/>
               
                <div className="input-field">
                        <span>Image</span>
                        <input type="file" id="image" name="image" accept="image/*" 
                        onChange={fileUpload} 
                        multiple 
                        ref={inputRef} 
                        // onChange={formik.handleChange} onBlur={formik.handleBlur} 
                        // value={formik.values.channel}
                        // {...formik.getFieldProps('image')}
                        />
                    {formik.errors.image&&formik.touched.image?<div className={styles.formError}>{formik.errors.image}</div>:''}
                </div>
                <br/><br/>
                <div className="input-field">
                    <label htmlFor="content">Content</label>
                    {/* <input type="text" id="content" name="content" 
                    onChange={formik.handleChange} onBlur={formik.handleBlur} value={formik.values.channel}
                    {...formik.getFieldProps('content')}
                    /> */}
                    <textarea  id="content" name="content" rows="4" {...formik.getFieldProps('content')}>
                    </textarea>

                    {formik.errors.content&&formik.touched.content?<div className={styles.formError}>{formik.errors.content}</div>:''}
                </div>
                <button className="waves-effect waves-light btn" type="submit">Submit</button>
            </form>
        </div>
    )
}

export default sarticle
