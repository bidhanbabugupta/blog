import Head from 'next/head'
import Category from '../components/Category'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
   <div>
   <Head>
    <title>LBG</title>
   </Head>
   <main>
    <div className={styles.poster}>
      MY BLOG
    </div>
    <Category/>
   </main>
   </div>
  )
}
