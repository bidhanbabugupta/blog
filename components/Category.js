import styles from './../styles/category.module.css'
function Category() {
    return (
        <div className={styles.category}>
            <ul className={styles.ul}>
                <li className={styles.li}>Life</li>
                <li className={styles.li}>Art</li>
                <li className={styles.li}>Technology</li>
                <li className={styles.li}>Mobile</li>
                <li className={styles.li}>React</li>
                <li className={styles.li}>Life</li>
                <li className={styles.li}>Art</li>
                <li className={styles.li}>Technology</li>
                <li className={styles.li}>Mobile</li>
                <li className={styles.li}>React</li>
            </ul>
        </div>
    )
}

export default Category
